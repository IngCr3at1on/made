cmake_minimum_required(VERSION 2.6)
project("MADE" C)

set(CMAKE_SYSTEM_NAME Linux)

# We need Vala macros for Cmake to make sense of the .vala source
list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake/vala)
# Include our listed macros
include(FindVala)
include(UseVala)

find_package(Vala REQUIRED)
find_package(PkgConfig REQUIRED)

pkg_check_modules(GTK REQUIRED gtk+-3.0)
pkg_check_modules(GTKSOURCEVIEW REQUIRED gtksourceview-3.0)

add_definitions(${GTK_CFLAGS})
add_definitions(${GTK_CFLAGS_OTHER})
add_definitions(${GTKSOURCEVIEW_CFLAGS})

link_libraries(${GTK_LIBRARIES})
link_libraries(${GTKSOURCEVIEW_LIBRARIES})

link_directories(${GTK_LIBRARY_DIRS})
link_directories(${GTKSOURCEVIEW_LIBRARY_DIRS})

add_subdirectory(src)

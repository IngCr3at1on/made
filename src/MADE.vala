using Gtk;

public MadeNotebook made_notebook;
public SourceLanguageManager language_manager;

public class MadeWindow : Grid {
	public MadeWindow() {
		made_notebook = new MadeNotebook();
		language_manager = new SourceLanguageManager();

		MenuBar menubar = new MenuBar();

		/* Both Menu and MenuItem exist in glib as well as gtk, so we
		 * define which to use. */
		Gtk.MenuItem item_file = new Gtk.MenuItem.with_label("File");
		Gtk.Menu filemenu = new Gtk.Menu();
		item_file.set_submenu(filemenu);
		menubar.add(item_file);

			Gtk.MenuItem item_new = new Gtk.MenuItem.with_label("New");
			filemenu.add(item_new);
			item_new.activate.connect(()=> {
				int tab = MadeNotebookFunctions.notebook_open_tab(null);
				MadeNotebookFunctions.notebook_set_focus(tab);
			});

			Gtk.MenuItem item_open = new Gtk.MenuItem.with_label("Open");
			filemenu.add(item_open);
			item_open.activate.connect(()=> {
				int tab = MadeNotebookFunctions.notebook_open_document(null);
				// Only set the page focus if we opened a new tab.
				if(tab != -1) MadeNotebookFunctions.notebook_set_focus(tab);
			});

			Gtk.MenuItem item_save = new Gtk.MenuItem.with_label("Save");
			filemenu.add(item_save);
			item_save.activate.connect(()=> {
				MadeNotebookFunctions.notebook_save_document();
			});

			Gtk.MenuItem item_save_as = new Gtk.MenuItem.with_label("Save As");
			filemenu.add(item_save_as);
			item_save_as.activate.connect(()=> {
				MadeNotebookFunctions.notebook_save_document_as();
			});

			Gtk.MenuItem item_close = new Gtk.MenuItem.with_label("Close");
			filemenu.add(item_close);
			item_close.activate.connect(()=> {
				MadeNotebookFunctions.notebook_close_tab();
			});

			Gtk.MenuItem item_exit = new Gtk.MenuItem.with_label("Exit");
			filemenu.add(item_exit);
			item_exit.activate.connect(()=> {
				MadeNotebookFunctions.notebook_close_all();
				Process.exit(0);
			});

		// Also generate a help menu.
		Gtk.MenuItem item_help = new Gtk.MenuItem.with_label("Help");
		Gtk.Menu helpmenu = new Gtk.Menu();
		item_help.set_submenu(helpmenu);
		menubar.add(item_help);

			Gtk.MenuItem item_about = new Gtk.MenuItem.with_label("About");
			helpmenu.add(item_about);
			item_about.activate.connect(()=> {
				MadeMenuFunctions.post_about();
			});

		// Generate a standard toolbar.
		Toolbar toolbar = new Toolbar();
		toolbar.set_hexpand(true);

			// Add a new button and set it to not display it's title.
			Image tb_img_new = new Image();
			tb_img_new.set_from_icon_name("gtk-new", IconSize.MENU);
			ToolButton tb_item_new = new ToolButton(tb_img_new, null);
			tb_item_new.set_is_important(false);
			toolbar.insert(tb_item_new, 0);
			tb_item_new.clicked.connect(()=> {
				int tab = MadeNotebookFunctions.notebook_open_tab(null);
				MadeNotebookFunctions.notebook_set_focus(tab);
			});

			// Do the same with our open button.
			Image tb_img_open = new Image();
			tb_img_open.set_from_icon_name("gtk-open", IconSize.MENU);
			ToolButton tb_item_open = new ToolButton(tb_img_open, null);
			tb_item_open.set_is_important(false);
			toolbar.insert(tb_item_open, 1);
			tb_item_open.clicked.connect(()=> {
				int tab = MadeNotebookFunctions.notebook_open_document(null);
				if(tab != -1) MadeNotebookFunctions.notebook_set_focus(tab);
			});

		// Attach everything to our window.
		this.attach(menubar, 0, 0, 1, 1);
		this.attach_next_to(
			toolbar,
			menubar,
			PositionType.BOTTOM,
			1,
			1
		);
		this.attach_next_to(
			made_notebook,
			toolbar,
			PositionType.BOTTOM,
			1,
			1
		);
	} // public MadeWindow
} // class MadeWindow

public class App : Gtk.Application {
	public App() {
		Object(
			application_id: "made.application",
			flags: ApplicationFlags.FLAGS_NONE
		);
	} // public App

	protected override void activate() {
		ApplicationWindow main_window = new ApplicationWindow(this);

		main_window.title = "MADE";
		main_window.border_width = 1;
		main_window.window_position = WindowPosition.CENTER;
		main_window.set_default_size(650, 725);

		MadeWindow window = new MadeWindow();
		main_window.add(window);
		main_window.show_all();
	} // protected override void activate

	public static int main(string[] args) {
		App made = new App();
		return made.run(args);
	} // public static int main
} // public class App

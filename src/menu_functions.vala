using Gtk;

namespace MadeMenuFunctions {
	public void post_about() {
		AboutDialog dialog = new AboutDialog();
		dialog.set_destroy_with_parent(true);
		//dialog.set_transient_for(window);
		dialog.set_modal(true);

		dialog.authors = {"Nathan Bass"};
		dialog.program_name = "MADE";
		dialog.comments = "Made Another Development Environment";
		dialog.copyright = "Copyright (c) 2013, Nathan Bass";
		dialog.version = "0.0.1";
		
		//dialog.license = "";
		//dialog.wrap_license = true;

		dialog.response.connect ((response_id) => {
			if(response_id == Gtk.ResponseType.CANCEL || response_id == Gtk.ResponseType.DELETE_EVENT) {
				dialog.hide_on_delete ();
			}
		});

		// Show the dialog.
		dialog.present();
	} // public void post_about
} // namespace MadeMenuFunctions

using Gtk;

public class MadeNotebook : Notebook {
	public MadeNotebook() {
		this.set_scrollable(true);
	} // public MadeNotebook
} // public class MadeNotebook

public class Tab : Grid {
	public Label tab_label = new Label("New Document");

	public void set_tab_label(string filepath) {
		string[] path_strings = filepath.split("/");
		int i = path_strings.length;

		tab_label.set_label(path_strings[i - 1]);
	} // void set_tab_label
} // public class Tab

namespace MadeNotebookFunctions {
	/* Generate a new notebook tab with a scrollable window housed
	 * inside of it and return the notebook tab number for that new tab.
	 */
	public int notebook_open_tab(string? filepath) {
		/* Create a widget and set it up to be a close button using the
		 * stock gtk icons */
		Button btn = new Button();
		btn.set_focus_on_click(false);
		btn.set_name("close-tab-button");

		Image image = new Image();
		image.set_from_icon_name("gtk-close", IconSize.MENU);
		btn.add(image);

		Tab tab = new Tab();

		if(filepath != null)
			tab.set_tab_label(filepath);

		/* Generate a grid and place both our tab label and close button
		 * inside. */
		Grid tab_grid = new Grid();
		tab_grid.attach(tab.tab_label, 0, 0, 1, 1);
		tab_grid.attach_next_to(btn, tab.tab_label, PositionType.RIGHT, 1, 1);

		// Generate a scrollable window and attach our text area to it.
		ScrolledWindow scrolled_window = new ScrolledWindow(null, null);
		scrolled_window.set_policy(
			PolicyType.AUTOMATIC,
			PolicyType.AUTOMATIC
		);
		scrolled_window.border_width = 1;

		/* Add a callback function for the close button we added a
		 * moment ago (this is done now because we've waited until after
		 * creating our scrolled window, could also have defined the
		 * usage of the scrolled_window var at an earlier point if we
		 * had wanted to list the callback sooner in the code). */
		btn.clicked.connect(() => {
			notebook_really_close_tab(made_notebook.page_num(scrolled_window));
		});

		/* Make sure the scrolled_window is set to fill the entire
		 * non-occupied area of the window. */
		scrolled_window.set_hexpand(true);
		scrolled_window.set_halign(Align.FILL);
		scrolled_window.set_vexpand(true);
		scrolled_window.set_valign(Align.FILL);

		// Create a new document from our documenthandler
		MadeDocument document = DocumentHandler.get_document(filepath);
		scrolled_window.add(document);

		/* Finally attach our tab to the main notebook so we can see and
		 * use it. */
		made_notebook.append_page(scrolled_window, tab_grid);
		made_notebook.set_tab_reorderable(scrolled_window, true);

		tab_grid.show_all();
		made_notebook.show_all();

		return made_notebook.page_num(scrolled_window);
	} // public void notebook_open_tab

	/* Open a file.
	 *
	 * Returns the page number of the newly generated tab, returns -1 in
	 * the event of a user cancelling the open process, subsequently
	 * cancelling geneation of a new tab.
	 *
	 *	TODO:
	 * 		Allow us to open more then 1 file at a time. */
	public int notebook_open_document(string? filepath) {
		int ret = 0;
		if(filepath == null) {
			var chooser = new FileChooserDialog(
				"Open File",
				new Window(),
				FileChooserAction.OPEN,
				"gtk-cancel",
				ResponseType.CANCEL,
				"gtk-open",
				ResponseType.ACCEPT
			);

			if(chooser.run() != ResponseType.ACCEPT) {
				chooser.close();
				ret = -1;
			} else {
				ret = notebook_open_tab(chooser.get_filename());
				chooser.close();
			}
		}
		return ret;
	} // public int notebook_open_document

	// Static call to save that we can call from either save or saveas.
		/* Set filepath to ~document temporarily to fix build, cause
		 * it's not yet being used, leave a similar function here though
		 * when finished that way files can be saved to a temporary file
		 * for recovery in case of crash. */
	private void notebook_really_save_document(string filepath = "~document", bool check_lang = false) {
		/* Need to change this as we will not always have focus, such as
		 * when closing a tab from the tab button while on a different
		 * page/tab. */
		int tab = made_notebook.get_current_page();
		DocumentHandler.document_save_file(filepath, tab, check_lang);
	} // private void notebook_really_save_document

	/* Save the document from our current tab to a chosen filepath.
	 * 
	 * TODO
	 * 		Prompt before saving over an already existing file.
	 * 		If this has been called from a close function than we don't
	 * 		need to check the language. */
	public void notebook_save_document_as() {
		if(made_notebook.get_n_pages() <= 0)
			return;

		string filepath;
		var chooser = new FileChooserDialog(
			"Save File",
			new Window(),
			FileChooserAction.SAVE,
			"gtk-cancel",
			ResponseType.CANCEL,
			"gtk-save",
			ResponseType.ACCEPT
		);

		if(chooser.run() != ResponseType.ACCEPT) {
			chooser.close();
			return;
		} else {
			filepath = chooser.get_filename();
			notebook_really_save_document(filepath, true);
			notebook_set_label(filepath);
			chooser.close();
        }
	} // public void notebook_save_document_as

	/* Save the document from our current tab (assuming it already has a 
	 * filepath).
	 *
	 *	TODO
	 * 		Allow for saving of multiple files (ONLY if they already
	 * 		have a filepath).
	 * 		Track the state of a file to tell if it really needs to be
	 * 		saved. */
	public void notebook_save_document() {
		if(made_notebook.get_n_pages() <= 0)
			return;

		int tab = made_notebook.get_current_page();
		/* Cast our page to a ScrolledWindow instead of a standard
		 * Widget. */
		ScrolledWindow page = (ScrolledWindow)made_notebook.get_nth_page(tab);
		/* Like the page cast the document to MadeDocument as opposed to
		 * default Widget. */
		MadeDocument document = (MadeDocument)page.get_child();

		/* Check the filepath in our document, if it's null than prompt
		 * the user w/ the save_as dialog, else go ahead and save the
		 * document, this will overwrite the old file without prompting.
		 */
		if(document.filepath == null) {
			notebook_save_document_as();
		} else {
			/* We don't need to check the language again cause the
			 * filepath hasn't been changed. */
			notebook_really_save_document(document.filepath);
		}
	} // public void notebook_save_document

	/* Really close out our current document.
	 * 
	 * 	TODO
	 * 		Call save function if the file is not already saved.
	 * 		Collapse/destroy all widgets housed in the specified tab
	 * 		(basically as it is the tab is removed from	view but the
	 * 		widgets are still in memory... This is likely no longer
	 * 		valid as we are building in vala instead of C...) */
	private void notebook_really_close_tab(int tab) {
        made_notebook.remove_page(tab);
	} // private void notebook_really_close_tab
	
	// Public call to close our current document.
	public void notebook_close_tab() {
        notebook_really_close_tab(made_notebook.get_current_page());
	} // public void notebook close tab

	// Public call to close all open tabs/documents.
	public void notebook_close_all() {
		// TODO: iterate through open documents closing each.
		notebook_close_tab();
	} // public void notebook_close_all

	/* Calling focus from within the open_tab function doesn't work,
	 * presumably because tab isn't set to be shown yet (not certain)
	 * to resolve this allow methods to call focus directly. */
	public void notebook_set_focus(int tab) {
		made_notebook.set_current_page(tab);
	} // public void notebook_set_focus

	// Set the tab label from the filepath if it exists.
	public void notebook_set_label(string filepath) {
		string[] path_strings = filepath.split("/");
		int i = path_strings.length;

		Tab tab = (Tab)made_notebook.get_tab_label(made_notebook.get_nth_page(made_notebook.get_current_page()));
		tab.set_tab_label(path_strings[i - 1]);
	} // public void notebook_set_label
} // namespace MadeNotebookFunctions

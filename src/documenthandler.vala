using Gtk;

// Expand standard SourceView to house MADE requirements.
public class MadeDocument : SourceView {
	public MadeDocument() {
		this.set_buffer(buffer);
	}

	public SourceBuffer buffer = new SourceBuffer(null);
	public string filepath = null;
} // public class MadeDocument

namespace DocumentHandler {
	// Get a new document (either blank or from a file).
	public MadeDocument get_document(string ? filepath = null) {
		MadeDocument document = new MadeDocument();

		if(filepath != null) {
			try {
				string text;
				FileUtils.get_contents(filepath, out text);
				document.buffer.text = text;
				document.filepath = filepath;
			} catch (Error e) {
				stderr.printf("Error :%s\n", e.message);
			}

			SourceLanguage language = language_manager.guess_language(filepath, null);
			document.buffer.set_language(language);
		}
		document.show_all();
		return document;
	} // public MadeDocument get_document

	// Save a file.
	public void document_save_file(string filepath, int tab, bool check_lang = false) {
		TextIter start, end;
		/* Cast our page to a ScrolledWindow instead of a standard
		 * Widget. */
		ScrolledWindow page = (ScrolledWindow)made_notebook.get_nth_page(tab);

		/* Like the page cast the document to MadeDocument as opposed to
		 * default Widget. */
		MadeDocument document = (MadeDocument)page.get_child();

		document.set_sensitive(false);

		// Cast buffer to SourceBuffer instead of default TextBuffer.
		SourceBuffer buffer = (SourceBuffer)document.get_buffer();
		buffer.get_start_iter(out start);
		buffer.get_end_iter(out end);
		string text = buffer.get_text(start, end, false);
		buffer.set_modified(false);
		document.set_sensitive(true);

		try {
			FileUtils.set_contents(filepath, text, -1);
		} catch (Error e) {
			stderr.printf("Error :%s\n", e.message);
		}

		/* Go ahead and check our language now that we've saved the doc.
		 * (should disable this when called from close by use of a
		 * callback) */
		if(check_lang == true) {
			bool nullres;
			/* I'm too lazy to find another way to do this so just guess
			 * the type off of the filepath. */
			string type = ContentType.guess(filepath, null, out nullres);
			if(nullres) {
				type = null;
			}
			SourceLanguage lang = language_manager.guess_language(filepath, type);
			buffer.set_language(lang);
		}
	}
} // namespace DocumentHandler

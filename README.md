# MADE
### Made Another Development Environment

Requirements:

	cmake 2.6 or later
	vala
	gtk+
	gtksourceview

The included submodule Vala_CMake is also required, fetch it w/ the following:

	git submodule init
	git submodule update

To compile this project after meeting the above dependencies perform an out-of-source build by 
entering:

	mkdir build
	cd build
	cmake ../
	make

This will compile a binary file in build/src/, run it with

	./src/MADE

